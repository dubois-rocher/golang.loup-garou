package reg

type Client struct {
	Pseudo string
	State int
	Vote int `json:"-"`
	Role string `json:"-"`
	Dead bool `json:"-"`
}

var IPClient map[string]Client = make(map[string]Client)

/*
* Methode permettant ajouter un pair dans le registre
*/
func Add(address string) bool {
	_, exist := IPClient[address]
	if !exist {
		IPClient[address] = Client{Pseudo: "", State: 0, Dead: false, Vote: -1}
		return true
	} else {
		return false
	}
}

/*
* Methode permettant de définir le pseudo d'un pair
*/
func SetPseudo(address string, pseudo string)  {
	pair := IPClient[address]
	pair.Pseudo = pseudo
	IPClient[address] = pair
}

/*
* Methode permettant de copier des maps
*/
func CopyMap(originalMap map[string]Client) map[string]Client {
	newMap := make(map[string]Client)
	for key, value := range originalMap {
		newMap[key] = value
	}
	return newMap
}

/*
* Methode permettant de récupérer la valeur max de vote dans le registre
* @return La valeur max ou -max si égalité entre les deux pairs max
*/
func GetMaxVoteValue() int {
	i := 0

	// On obtient la plus grosse valeur
	for _, cli := range IPClient {
		if cli.Vote > i {
			i = cli.Vote
		}
	}

	first := false

	// On vérifie qu'elle est unique
	for _, cli := range IPClient {
		if cli.Vote == i {
			if first == true {
				i = -i
				break
			} else {
				first = true
			}
		}
	}

	return i
}
