package main

import (
	"loup-garou/reg"
	"bufio"
    "bytes"
	"encoding/json"
	"fmt"
	"net"
	"regexp"
	"strconv"
	"strings"
	"math/rand"
	"time"
	"os"
	"sync"
)

var (
	// Connexions et autres
	ip_host string
	ip_for_friend string
	pseudo string = ""
	state int = 0

	// Roles
	role string = ""
	rolesDispos = []string {"loup", "sorciere", "voyante", "chasseur", "loup", "villageois", "villageois", "loup", "villageois"}

	// Variables pour la boucle de jeu
	VillaChoice map[string]string
	WolfChoice map[string]string
	I_AM_DEAD bool = false
	morts map[string]string = make(map[string]string)

	// Variables de sorciere
	savedSomeone bool = false
	killedSomeone bool = false

	// Protection du registre
	mux sync.Mutex
)

const (
	// Etats
	STATE_READY 				= 1
	STATE_SEND_ELECTION_NUMBER	= 2
	STATE_RESTART_ELECTION		= 3
	STATE_NIGHT 				= 4
	STATE_VOYANTE_DONE 			= 5
	STATE_WOLFES_DONE 	    	= 6
	STATE_SORCIERE_DONE 	    = 7
	STATE_CHASSEUR_NIGHT_DONE 	= 8
	STATE_DAY_DONE 				= 9
	STATE_CHASSEUR_DAY_DONE		= 10
	STATE_EVERYONE_DONE 		= 11
	STATE_GAME_OVER		 		= 99
)

// Boucle principale du programme
func main() {

	fmt.Print("\nLoup Garou Golang PeerToPeer\n")

	ip_host = "localhost"
	ip_for_friend = GetIpAddress() // On récupere son addresse ip

	createParty := AskForAnswer("Souhaitez-vous créer ou rejoindre une partie ? [créer/rejoindre] ") == "créer"

	for pseudo == "" {
		pseudo = AskForAnswer("Entrez votre pseudo : ")
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if createParty == true {			// Créer une partie ou Rejoindre une partie ?
		DisplayAtTheBeginning("\n	Création d'une partie en cours...\n")
	} else {
		DisplayAtTheBeginning("\n	Accès à une partie en cours...\n")
		ip_host = AskForAnswer("Entrez l'adresse IP de votre ami : ")

		// On se connecte au réseau via l'host qu'on connait
		conn := SendRequestToSomeone(ip_host, "req-new-conn", pseudo)	// Envoi requete
		accepted := GestionDunPair(conn)								// Ecoute réponse
		if !accepted {
			return
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	go GestionDesConnexionEntrantes()	// On créer un serveur sur lequel des pairs peuvent nous rejoindre
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	WaitForReady() 						// On attend que l'utilisateur soit pret
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	SyncRegistre() 						// On synchronise tous les registres afin que tous le monde connaisse tous le monde
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Elections() 						// On fais l'élection du leader et on définit les rôles
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	



	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//												       DEMARRAGE DU JEU													//
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	GAME_LOOP: for {
		/////////////////////////////////////////////////////////////////////////////// C'est la nuit
		if EtatDesMorts() {
			break GAME_LOOP // Fin du jeu
		}

		// Initialisation des variables
		WolfChoice = make(map[string]string)	// On réinitilise les vecteurs de vote à zero 
		VillaChoice = make(map[string]string)	// On réinitilise les vecteurs de vote à zero 
		morts = map[string]string{
			"villageois" : "",
			"loup": "",
			"sorciere": "",
			"chasseur": "",
		}

		//**********************************************************************************************************************//
		SendStateToEveryone(STATE_NIGHT)					// On envoie l'état "NIGHT"
		WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_NIGHT, STATE_VOYANTE_DONE, STATE_WOLFES_DONE})		// On attend que tous les pairs soit sur l'état "NIGHT"
		//**********************************************************************************************************************//

		//  ------------------------------------------------------------------------------------------------------------------------------------ La voyante //
			LaVoyanteSeReveille()								// DEV BY : Nathan ROCHER
		//  ------------------------------------------------------------------------------------------------------------------------------------ La voyante //

		//  ------------------------------------------------------------------------------------------------------------------------------------ Les loups-garous //
			LesLoupsSeReveillent()								// DEV BY : Eliott DUBOIS & Nathan ROCHER
		//  ------------------------------------------------------------------------------------------------------------------------------------ Les loups-garous //
		
		//  ------------------------------------------------------------------------------------------------------------------------------------ La sorcière //
			LaSorciereSeReveille()								// DEV BY : Eliott DUBOIS
		//  ------------------------------------------------------------------------------------------------------------------------------------ La sorcière //
		
		//  ------------------------------------------------------------------------------------------------------------------------------------ Le chasseur //
			LeChasseurVientDeMourir(STATE_CHASSEUR_NIGHT_DONE)	// DEV BY : Nathan ROCHER
		//  ------------------------------------------------------------------------------------------------------------------------------------ Le chasseur //
		
		if EtatDesMorts() {
			break GAME_LOOP // Fin du jeu
		}

		ToutLeMondeSeReveille()		// Vote du jour

		//**********************************************************************************************************************//
		SendStateToEveryone(STATE_DAY_DONE)					// On envoie l'état "DAY"
		WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_DAY_DONE, STATE_CHASSEUR_DAY_DONE, STATE_EVERYONE_DONE, STATE_NIGHT})	// On attend que tous les pairs soit sur l'état "DAY"
		//**********************************************************************************************************************//
		
		morts = map[string]string{
			"villageois" : WhoIsDead(VillaChoice), // Qui est mort apres le choix des villageois
			"loup": "",
			"sorciere": "",
			"chasseur": "",
		}
		
		//  ------------------------------------------------------------------------------------------------------------------------------------ Le chasseur //
		LeChasseurVientDeMourir(STATE_CHASSEUR_DAY_DONE)		// DEV BY : Nathan ROCHER
		//  ------------------------------------------------------------------------------------------------------------------------------------ Le chasseur //
		
		//**********************************************************************************************************************//
		SendStateToEveryone(STATE_EVERYONE_DONE)											// On envoie l'état "EVERYONE_DONE"
		WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_EVERYONE_DONE, STATE_NIGHT})		// On attend que tous les pairs soit sur l'état "EVERYONE_DONE" ou "STATE_NIGHT"
		//**********************************************************************************************************************//

	}

	SendStateToEveryone(STATE_GAME_OVER)					// On envoie l'état "GAME_OVER"
	WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_GAME_OVER, STATE_SORCIERE_DONE})		// On attend que tous les pairs soit sur l'état "GAME_OVER"
	
	fmt.Println("\n == Jeux terminé == ")

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											SOUS FONCTIONS DE LA BOUCLE DE JEU											//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* RÔLE   : SORCIERE
* DEV BY : Eliott DUBOIS
*/
func LaSorciereSeReveille()  {
	// Si la sorciere est en vie
	if !EveryoneDied("sorciere") {
		
		if !savedSomeone || !killedSomeone { // Si la sorciere peut encore effectuer au moins une des deux actions
			fmt.Println("\n--> La sorciere se reveille")
	
			if role == "sorciere" && !I_AM_DEAD {
				didISavedSomeOne := false
	
				// SAUVER QUELQU'UN
				if !savedSomeone { // Si la sorciere n'a pas déjà sauver quelqu'un
					le_mort := morts["loup"]
					if le_mort != "" { // Si les loups ont tué quelqu'un
						SauverQlqn: for {
							rep_saving := ""
							if le_mort == ip_for_friend {
								rep_saving = AskForAnswer("On essaye de te tuer !! Tu te soignes ?! (o/n) : ")
							} else {
								rep_saving = AskForAnswer("Voulez-vous sauver " + reg.IPClient[le_mort].Pseudo + " ? (o/n) : ")
							}

							if rep_saving == "o" || rep_saving == "O" || rep_saving == "oui" || rep_saving == "Oui" {
								savedSomeone = true
								didISavedSomeOne = true
								SendToEveryone("saving", le_mort) // Envoi a tout le monde de ne pas le tuer
								morts["loup"] = ""
	
								break SauverQlqn // On quitte la boucle SauverQlqn
							} else if rep_saving == "n" || rep_saving == "N" || rep_saving == "non" || rep_saving == "Non" {
								break SauverQlqn // On quitte la boucle SauverQlqn
							} else { 
								fmt.Println("--> Vos propos sont incohérents... :/")
							}
						}
					}
				}
	
				// TUER QUELQU'UN
				if !didISavedSomeOne && !killedSomeone { // Si la sorciere n'a pas déjà tuer quelqu'un
					PrintPeopleAliveNotOfRole("all") // On affiche la liste des joueurs en vie
					
					AskKilling: for {
						rep_killing := AskForAnswer("Souhaitez vous tuer quelqu'un ? (o/n) : ")
		
						if rep_killing == "o" || rep_killing == "O" || rep_killing == "oui" || rep_killing == "Oui" {
							TuerQlqn: for {
								killOfSorciere := AskForAnswer("Quelle personne souhaitez-vous tuer ? : ")
								
								if _, ok := reg.IPClient[killOfSorciere]; ok { // Si le joueur existe
									killedSomeone = true
									fmt.Println("--> Vous avez décidé de tuer : " + reg.IPClient[killOfSorciere].Pseudo)
									KillSomeone("sorciere", killOfSorciere) // Envoi a tout le monde de le tuer
									break TuerQlqn // On quitte la boucle TuerQlqn
								} else {
									fmt.Println("--> Ce joueur n'existe pas")
								}
							}

							break AskKilling // On quitte la boucle AskKilling

						} else if rep_killing == "n" || rep_killing == "N" || rep_killing == "non" || rep_killing == "Non" {
							break AskKilling // On quitte la boucle AskKilling
						} else { 
							fmt.Println("--> Vos propos sont incohérents... :/")
						}
					}
				}
			}
			
			//**********************************************************************************************************************//
			SendStateToEveryone(STATE_SORCIERE_DONE)					// On envoie l'état "STATE_SORCIERE_DONE"
			WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_SORCIERE_DONE, STATE_CHASSEUR_NIGHT_DONE, STATE_DAY_DONE, STATE_GAME_OVER})	// On attend que tous les pairs soit sur l'état "STATE_SORCIERE_DONE"
			//**********************************************************************************************************************//
		}
	}
}

/*
* RÔLE   : CHASSEUR
* DEV BY : Nathan ROCHER
*/
func LeChasseurVientDeMourir(afterState int) {
	dead := false

	for _, valeur := range morts {
		if valeur == ip_for_friend {
			dead = true
		}
	}

	if role == "chasseur" && dead == true { // Si je suis le chasseur
		
		if state < STATE_EVERYONE_DONE {
			fmt.Println("\n--> Vous êtes mort durant la nuit")
		} else {
			fmt.Println("\n--> Vous êtes mort durant le jour")
		}

		fmt.Println("--> Comme vous étiez chasseur, vous pouvez tuer une personne")
		PrintPeopleAliveNotOfRole("all") // On affiche la liste des joueurs en vie
	
		RoleChasseur: for {
			ChasseurIP := AskForAnswer("Quelle personne souhaitez-vous tuer ? Entrer l'IP : ") // On demande la personne qu'il veut tuer

			if _, ok := reg.IPClient[ChasseurIP]; ok { // Si le joueur existe
				fmt.Println("--> Vous avez décidé de tuer : " + reg.IPClient[ChasseurIP].Pseudo)
				KillSomeone("chasseur", ChasseurIP) // On envoit a tous le monde qu'on vient de tuer cette personne
				break RoleChasseur // On quitte la boucle du RoleChasseur
			} else {
				fmt.Println("--> Ce joueur n'existe pas")
			}
		}

	}

	//**********************************************************************************************************************//
	SendStateToEveryone(afterState)					// On envoie l'état "CHASSEUR_OK" Day or night
	WaitingForEveryoneOnTheSpecifiedStates([]int{afterState, afterState+1})	// On attend que tous les pairs soit sur l'état "CHASSEUR_OK" Day or night
	//**********************************************************************************************************************//
}

/*
* RÔLE   : VOYANTE
* DEV BY : Nathan ROCHER
*/
func LaVoyanteSeReveille() {
	// Si la voyante est en vie
	if !EveryoneDied("voyante") {

		fmt.Println("\n--> La voyante se reveille")

		if role == "voyante" && !I_AM_DEAD { // Si je suis la voyante

			PrintPeopleAliveNotOfRole("all") // On affiche la liste des joueurs en vie

			RoleVoyante: for {
				voyante_ip := AskForAnswer("Quelle personne souhaitez-vous voir ? Entrer l'IP : ") // On demande la personne qu'il veut voir

				if _, ok := reg.IPClient[voyante_ip]; ok { // Si le joueur existe
					fmt.Println("--> Vous avez décidé de voir la carte de : " + reg.IPClient[voyante_ip].Pseudo) 
					fmt.Println("--> " + reg.IPClient[voyante_ip].Pseudo + " à la carte : " + reg.IPClient[voyante_ip].Role) // On affiche sa carte
					break RoleVoyante // On quitte la boucle RoleVoyante
				} else {
					fmt.Println("--> Ce joueur n'existe pas")
				}
			}
		}

		//**********************************************************************************************************************//
		SendStateToEveryone(STATE_VOYANTE_DONE)					// On envoie l'état "VOYANTE_DONE"
		WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_VOYANTE_DONE, STATE_WOLFES_DONE})	// On attend que tous les pairs soit sur l'état "VOYANTE_DONE"
		//**********************************************************************************************************************//
	}
}

/*
* RÔLE   : VILLAGOIS - Tout le monde
* DEV BY : Nathan ROCHER & Eliott DUBOIS
*/
func ToutLeMondeSeReveille()  {
	// Si on est en vie
	if !I_AM_DEAD {
		fmt.Println("\n--> Le village se réveille")

		fmt.Println("\t\t=== Démarrage du chat global ===")

		PrintPeopleAliveNotOfRole("all")  // On affiche les joueurs encore en vie

		// On démarre le tchat pour tous les joueurs en vie
		ChatVilla: for {
			ans_villa := AskForAnswer("Chat global (!pret/!joueurs/!kill@@@{ip}) ou votre message : ")

			args_villa := strings.Split(ans_villa, "@@@")// On parse le texte qu'il a écrit

			switch(args_villa[0]) {// On vérifie si il a écrit une commande
				case "!pret":// Si il est pret, on quitte le tchat
					break ChatVilla
				case "!kill": // Si il est kill quelqu'un, on le partage a tous le monde
					if _, ok := reg.IPClient[args_villa[1]]; ok { // Si le joueur existe
						fmt.Println("--> Vous avez décidé de tuer : " + args_villa[1])
						VillaChoice[ip_for_friend] = args_villa[1] // On enregistre le vote
						SendToEveryone("chat", "!i-kill@@@" + args_villa[1]) // On envoie le vote
					} else {
						fmt.Println("--> Ce joueur n'existe pas")
					}
				case "!joueurs":
					PrintPeopleAliveNotOfRole("all")  // On affiche les joueurs encore en vie
				default: // Par défault, si la commande n'est pas reconnu, on envoie le texte à tous les autres
					if ans_villa != "" {
						fmt.Println("::: Vous : ", ans_villa)
						SendToEveryone("chat", "!message@@@" + ans_villa) // On envoie le texte
					}
			}
		}

		fmt.Println("\n--> Attente de tous les autres joueurs")
	}
}

/*
* RÔLE   : LOUPS GAROUS
* DEV BY : Eliott DUBOIS & Nathan ROCHER
*/
func LesLoupsSeReveillent()  {
	fmt.Println("\n--> Les loups-garous se réveillent")
	
	// Si on est loup-garou et en vie
	if role == "loup" && !I_AM_DEAD {

		PrintPeopleAliveNotOfRole("loup")  // On affiche les joueurs encore en vie

		// Démarrage du chat :
		fmt.Println("\t\t=== Démarrage du chat pour les loups garous ===")

		// On démarre le tchat des loups-garou
		ChatLoup: for {
			ans_wolf := AskForAnswer("Chat Loup-Garou (!pret/!joueurs/!kill@@@{ip}) ou votre message : ")

			args_wolf := strings.Split(ans_wolf, "@@@") // On parse le texte qu'il a écrit

			switch args_wolf[0] { // On vérifie si il a écrit une commande
				case "!pret": // Si il est pret, on quitte le tchat
					break ChatLoup
				case "!kill": // Si il est kill quelqu'un, on le partage a tous le monde
					if _, ok := reg.IPClient[args_wolf[1]]; ok { // Si le joueur existe
						fmt.Println("--> Vous avez décidé de tuer : " + args_wolf[1])
						WolfChoice[ip_for_friend] = args_wolf[1] // On enregistre le vote
						SendToEveryone("chat-lp", "!i-kill@@@" + args_wolf[1]) // On envoie le vote
					} else {
						fmt.Println("--> Ce joueur n'existe pas")
					}
				case "!joueurs":
					PrintPeopleAliveNotOfRole("loup") // On affiche les joueurs encore en vie
				default: // Par défault, si la commande n'est pas reconnu, on envoie le texte a tous les autres loup-garou
					if ans_wolf != "" {
						SendToEveryone("chat-lp", "!message@@@" + ans_wolf) // On envoie le texte
					}
			}
		}
	}

	//**********************************************************************************************************************//
	SendStateToEveryone(STATE_WOLFES_DONE)					// On envoie l'état "WOLFES_DONE"
	WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_WOLFES_DONE, STATE_SORCIERE_DONE, STATE_CHASSEUR_NIGHT_DONE, STATE_DAY_DONE})		// On attend que tous les pairs soit sur l'état "WOLFES_DONE"
	//**********************************************************************************************************************//

	morts["loup"] = WhoIsDead(WolfChoice) // On réalise le vote pour la personne qui est morte
}

/*
* Affichage JOUR/NUIT
* Etat des morts
*/
func EtatDesMorts() bool {
	stop_game := false

	// Clear le terminal
	os.Stdout.WriteString("\x1b[3;J\x1b[H\x1b[2J")
	/////////////////////////////////////////////////////////////////////////////// C'est le jour/nuit
	if state < STATE_NIGHT || state == STATE_EVERYONE_DONE {
		fmt.Println("\n\n\t\tLa nuit tombe")
	} else {
		fmt.Println("\n\n\t\tLe jour se lève")
	}

	if state > STATE_RESTART_ELECTION { // SI on est bien dans la boucle de jeu (évite le premier display)
		//  ------------------------------------------------------------------------------------------------------------------------------------ Etat des morts
		IfSomeoneIsDead()
		
		every_loups_are_dead, every_villageois_are_dead := WhoWin()
		if every_villageois_are_dead { // Si tous les villageois sont mort
			fmt.Println("\n\n\t\tLes loups ont gagné !")
			stop_game = true
		}
		if every_loups_are_dead { // Si tous les loups sont mort
			fmt.Println("\n\n\t\tLes villageois ont gagné !")
			stop_game = true
		}
		//  ------------------------------------------------------------------------------------------------------------------------------------ Etat des morts
	}
	return stop_game
}

/*
*
*/
func KillSomeone(role string, ip string) {
	mux.Lock()
	pair := reg.IPClient[ip]
	pair.Dead = true
	reg.IPClient[ip] = pair
	mux.Unlock()
	morts[role] = ip
	SendToEveryone("kill@@@" + role, ip)
}

/*
* Methode permettant d'afficher des informations
* @return l'adresse ip de la pesonne morte
*/
func IfSomeoneIsDead() {
	etat_des_morts := ""
	count_morts := 0

	for role_killer, ip_mort := range morts {
		if ip_mort != "" {
			count_morts = count_morts + 1
			if ip_mort == ip_for_friend {
				etat_des_morts = etat_des_morts + "\n\t- Args... ils m'ont eu... "
			} else {
				if state < STATE_EVERYONE_DONE {
					etat_des_morts = etat_des_morts + "\n\t- " + reg.IPClient[ip_mort].Pseudo + " n'a pas survécu."
				} else {
					switch role_killer {
					case "villageois":
						etat_des_morts = etat_des_morts + "\n\t- Les aventuriers de la tribu réunifiée ont décidé d'éliminer " + reg.IPClient[ip_mort].Pseudo + "."
					case "chasseur":
						for _, pair := range reg.IPClient {
							if pair.Role == "chasseur" {
								etat_des_morts = etat_des_morts + "\n\t- Dans son dernier soupir, " + pair.Pseudo + " le chasseur a tué " + reg.IPClient[ip_mort].Pseudo + "."
								break
							}
						}
					}
				}
			}

			if ip_mort != ip_for_friend {
				mux.Lock()
				pair := reg.IPClient[ip_mort]
				pair.Dead = true
				reg.IPClient[ip_mort] = pair
				mux.Unlock()
			} else {
				I_AM_DEAD = true
			}
		}
	}

	jour_nuit := ""

	if state < STATE_EVERYONE_DONE {
		jour_nuit = "cette nuit"
	} else {
		jour_nuit = "aujourd'hui"
	}

	etat_des_morts = "\n--> " + strconv.Itoa(count_morts) + " personne(s) est/sont morte(s) " + jour_nuit + ": " + etat_des_morts

	if count_morts > 0 {
		fmt.Println(etat_des_morts)
	} else {
		fmt.Println("\n--> Aucune victime n'est a déplorée.")
	}
}

/*
* Methode permettant de savoir qui est mort apres un vote
* @return l'adresse ip de la pesonne morte
*/
func WhoIsDead(tab map[string]string) string {
	killed := ""

	for _, peer_killed := range tab {
		killed = killed + peer_killed + ","
	}

	maxIt := 0
	final_killed := ""

	for _, peer_killed := range tab {
		if(strings.Count(killed, peer_killed) > maxIt){
			maxIt = strings.Count(killed, peer_killed)
			final_killed = peer_killed
		}
	}

	return final_killed
} 


/*
* Methode permettant de savoir si tous les joueurs d'un groupe sont mort ou non
* @return vrai ou faux
*/
func EveryoneDied(RoleVerif string) bool {
	mux.Lock()
	for _, pair := range reg.IPClient {
		if pair.Role == RoleVerif && !pair.Dead {
			mux.Unlock()
			return false
		}
	}
	mux.Unlock()

	if role == RoleVerif && !I_AM_DEAD {
		return false
	}

	return true
}

/*
* Methode permettant de savoir quel groupe a gagné
* @return tuple(bool, bool) avec (loup, villageois)
*/
func WhoWin() (bool, bool) {
	loups_mort := true
	villageois_mort := true

	mux.Lock()
	for _, pair := range reg.IPClient {
		if pair.Role == "loup" && !pair.Dead {
			loups_mort = false
		}
		if pair.Role != "loup" && !pair.Dead {
			villageois_mort = false
		}
	}
	mux.Unlock()
	if !I_AM_DEAD {
		if role == "loup" {
			loups_mort = false
		} else {
			villageois_mort = false
		}
	}

	return loups_mort, villageois_mort
}

/*
* Methode permettant de demander a l'utilisateur d'écrire
* title:string => Message affiché dans la console
* @return string => la réponse
*/
func PrintPeopleAliveNotOfRole(roleNonPrisEnCompte string) {
	fmt.Println("\tListes des joueurs encore en vie")

	mux.Lock()
	for ip, pair := range reg.IPClient {
		if pair.Role != roleNonPrisEnCompte && pair.Dead == false {
			fmt.Println("-->", pair.Pseudo, ":", ip)
		}
	}
	mux.Unlock()
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												GESTION DES CONNEXIONS													//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* Methode permettant de lancer le serveur
*/
func GestionDesConnexionEntrantes() {

	listener, err := net.Listen("tcp", ":6666")
	if err != nil {
		fmt.Println("Erreur lors du démarrage du serveur local\n", err)
	} else {
		for {
			conn, err := listener.Accept()
			if err == nil {
				go GestionDunPair(conn)
			} else {
				conn.Close()
			}
		}
	}
}

/*
* Fonction permettant de gérer les messages entrants
*/
func GestionDunPair(conn net.Conn) bool {
	var ip_remote string = GetIpFromConn(conn.RemoteAddr())

	// On ouvre un Reader
	reader := bufio.NewReader(conn)
	message, _ := reader.ReadBytes('\n')

	if string(message) != "" {

		// Si il y a un message, on le parse
		message = bytes.TrimSuffix(message, []byte{10})
		Parsed := bytes.Split(message, []byte{'@', '@', '@'})
		CMD := string(Parsed[0])
		VALUE := Parsed[1]

		switch CMD {
			case "!req-new-conn":
				fmt.Println("\n> Nouvelle connexion de ", ip_remote, " !!")
				fmt.Print("Tapez 'prêt' lorsque vous l'êtes : ")
				if state == 0 {
					if ip_host != "localhost" {
						SendResponseToSomeone(conn, "res-new-conn", ip_host)
					} else {
						mux.Lock()
						reg.Add(ip_remote)
						reg.SetPseudo(ip_remote, string(VALUE))
						mux.Unlock()
						SendResponseToSomeone(conn, "res-new-conn", "%imyourhost%@@@"+pseudo)
					}
				} else {
					SendResponseToSomeone(conn, "res-new-conn", "%rejected%")
				}
				break;
			case "!res-new-conn":
				conn.Close()

				if string(VALUE) != "%rejected%" {

					if string(VALUE) != "%imyourhost%" {
						ip_host = string(VALUE)
						connx := SendRequestToSomeone(ip_host, "req-net-conn", pseudo)
						GestionDunPair(connx)
					} else {
						pseudo_recu := string(Parsed[2])
						mux.Lock()
						reg.Add(ip_remote)
						reg.SetPseudo(ip_remote, pseudo_recu)
						mux.Unlock()
					}
					return true
				}
				break;

			case "!req-net-conn":
				SendResponseToSomeone(conn, "res-net-conn", pseudo)
				mux.Lock()
				reg.Add(ip_remote)
				reg.SetPseudo(ip_remote, string(VALUE))
				mux.Unlock()
				break;
			case "!res-net-conn":
				conn.Close()
				mux.Lock()
				reg.Add(ip_remote)
				reg.SetPseudo(ip_remote, string(VALUE))
				mux.Unlock()
				break;

			case "!req-send-registre":
				conn.Close()
				mux.Lock()
				pair := reg.IPClient[ip_remote]
				pair.State = STATE_READY
				reg.IPClient[ip_remote] = pair
				mux.Unlock()
				ReceiveRegister(ip_remote, VALUE)
				break;

			case "!req-set-state":
				mux.Lock()
				pair := reg.IPClient[ip_remote]
				i, _ := strconv.Atoi(string(VALUE))
				pair.State = i
				reg.IPClient[ip_remote] = pair
				mux.Unlock()
				SendResponseToSomeone(conn, "res-set-state", string(state))
				break;
			case "!res-set-state":
				conn.Close()
				mux.Lock()
				pair := reg.IPClient[ip_remote]
				i, _ := strconv.Atoi(string(VALUE)) // On parse la valeur recu en int
				pair.State = i // On définit sa valeur pour le state
				reg.IPClient[ip_remote] = pair
				mux.Unlock()
				break;

			case "!req-vote":
				conn.Close()
				n, _ := strconv.Atoi(string(VALUE)) // On parse la valeur recu en int
				mux.Lock()
				pair := reg.IPClient[ip_remote]
				pair.Vote = n // On définit sa valeur pour le vote
				reg.IPClient[ip_remote] = pair
				mux.Unlock()
				break;

			case "!role":
				conn.Close()
				roles := strings.Split(string(VALUE), ";")
				NoteRoles(roles) // Permet de parser et de définir les roles dans le registre
				break;

			case "!saving":
				conn.Close()
				morts["loup"] = ""
				savedSomeone = true
				break;
			
			case "!kill": // @@@role@@@mort
				conn.Close()
				morts[string(VALUE)] = string(Parsed[2])
				if string(Parsed[1]) == "sorciere" {
					killedSomeone = true
				}
				break;

			case "!chat-lp":
				conn.Close()
				switch(string(VALUE)) { // Si un loup-garou effectue une action dans le tchat, on le recoit ici
					case "!i-kill": // Si il veut tuer une personne
						WolfChoice[ip_remote] = string(Parsed[2])
						if role == "loup" {
							fmt.Println("-->", ip_remote, " a décidé de tuer : " + string(Parsed[2]))
						}
					case "!message": // Si il écrit un message
						if role == "loup" {
							fmt.Println(":::", ip_remote, ":", string(Parsed[2]))
						}
				}
				break;
			case "!chat":
				conn.Close()
				switch(string(VALUE)) { // Si une personne effectue une action dans le tchat, on le recoit ici
					case "!i-kill":// Vote pour tuer une personne
						VillaChoice[ip_remote] = string(Parsed[2])
						fmt.Println("-->", ip_remote, " a décidé de tuer : " + string(Parsed[2]))
					case "!message": // Si il écrit un message
						fmt.Println(":::", ip_remote, ":", string(Parsed[2]))
				}
				break;
			default :
				conn.Close()
				fmt.Println("COMMANDE NON RECONNU : ", CMD)
				fmt.Println("        IP EMMETRICE : ", ip_remote)
				fmt.Println("              VALEUR : ", string(VALUE))
				fmt.Println()
				break;
		}
	}
	return false
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												GESTION DU REGISTRE														//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* Methode permettant de convertir le registre en JSON et de l'envoyer sur une conn
* @return conn 
*/
func SendRegister(address string) net.Conn {
	conn, err := net.Dial("tcp", address + ":6666")
	if err != nil {
		fmt.Println("Erreur lors de la connexion avec le pair ", address, " :: ", err)
		return nil
	}
	writer := bufio.NewWriter(conn)
	// Ajout dans le registre de soi-même
	map_to_send := reg.CopyMap(reg.IPClient)

	// Conversion de la structure en JSON
	bytes_data, _ := json.Marshal(map_to_send)

	// Envoie du registre en json
	writer.WriteString("!req-send-registre@@@")
	writer.Write(bytes_data)
	writer.WriteByte('\n')
	writer.Flush()

	return conn
}

/*
* Methode permettant de parser un registre et de le convertir en variable mémoire
*/
func ReceiveRegister(ip_remote string, VALUE []byte) bool {
	var new_registre map[string]reg.Client = make(map[string]reg.Client)
	err := json.Unmarshal(VALUE, &new_registre)

	if err == nil {
		// Union avec le registre reçu
		for ip_pair, pair := range new_registre {
			if ip_pair != ip_for_friend {
				mux.Lock()
				reg.IPClient[ip_pair] = pair
				mux.Unlock()
			}
		}
	} else {
		fmt.Println("Erreur lors du parssage du registre.")
		return false
	}
	return true
}

/*
*
*/
func SyncRegistre()  {
	if ip_host != "localhost" {
		conn := SendRegister(ip_host)
		if conn != nil {
			GestionDunPair(conn)
		}
	}

	WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_READY})

	if ip_host == "localhost" {
		mux.Lock()
		for ip_pair, _ := range reg.IPClient {
			SendRegister(ip_pair)
		}
		mux.Unlock()
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												GESTION DES MESSAGES													//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* Methode permettant d'envoyer un message a une seule personne
*/
func SendRequestToSomeone(address string, command string, value string) net.Conn {
	conn, err := net.Dial("tcp", address + ":6666")
	if err != nil {
		fmt.Println("Erreur lors de la connexion avec le pair ", address, " :: ", err)
		return nil
	}
	writer := bufio.NewWriter(conn)
	writer.WriteString("!" + command + "@@@" + value + "\n")
	writer.Flush()
	return conn
}

/*
* Methode permettant d'envoyer une commande à une personne
*/
func SendResponseToSomeone(conn net.Conn, command string, value string) {
	writer := bufio.NewWriter(conn)
	writer.WriteString("!" + command + "@@@" + value + "\n")
	writer.Flush()
}

/*
* Methode permettant d'envoyer a chaque pair un état
* A utiliser avec WaitingForEveryoneOnTheSpecifiedStates
*/
func SendStateToEveryone(state_to_send int) {
	state = state_to_send
	SendToEveryone("res-set-state", strconv.Itoa(state_to_send))
}

/*
* Methode permettant d'envoyer une commande a tous le monde
*/
func SendToEveryone(command string, value string) {
	mux.Lock()
	for ip_pair, _ := range reg.IPClient {
		SendRequestToSomeone(ip_pair, command, value)
		time.Sleep(100 * time.Millisecond)
	}
	mux.Unlock()
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														GESTION DES ETATS												//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* Methode permettant d'attendre que l'utilisateur marque pret dans son client
* METHODE BLOCANTE
*/
func WaitForReady() {
	for state == 0 {
		ret := AskForAnswer("Tapez 'prêt' lorsque vous l'êtes : ")
		if ret == "prêt" || ret == "pret" {
			state = STATE_READY
		}
	}
}

/*
* Methode permettant d'attendre que chaque pair soit dans un des états spécifiés
*/
func WaitingForEveryoneOnTheSpecifiedStates(the_states []int) {
	all_peers_ok := false
	for !all_peers_ok {
		all_peers_ok = true
		mux.Lock()
		for _, pair := range reg.IPClient {
			peer_ok := false
		 	for _, value := range the_states {
				if pair.State == value {
					peer_ok = true
				}
			}
			if !peer_ok {
				all_peers_ok = false
			}
		}
		mux.Unlock()
		time.Sleep(50 * time.Millisecond)
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//											GESTION DU LEADER ET DES ROLES												//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* Methode permettant d'élir le Leader
*/
func Elections()  {
	LEADER := false
	rand.Seed(time.Now().UnixNano())

	for {
		// Génération de notre nombre
		random := rand.Intn(999999)
		chiffreLeader := strconv.Itoa(random)
	
		// On l'envoie et on attend les réponses
		SendToEveryone("req-vote", chiffreLeader)

		// A envoyé son nombre
		SendStateToEveryone(STATE_SEND_ELECTION_NUMBER)
		time.Sleep(100 * time.Millisecond)

		// On attend que tous les pairs génère leur nombre
		WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_SEND_ELECTION_NUMBER})

		mux.Lock()
		mv := reg.GetMaxVoteValue() // SI mv > 0 ALORS un pair à la valeur max SINON si mv < 0 ALORS 2 pair ou plus on la meme valeur
		mux.Unlock()

		// Si mv est négatif
		if mv < 0 {
			if -mv > random  || -mv == random { // Ce client n'a pas mieux donc
				// On est pas leader
				// Mais On refait un tour d'élection de leader
				SendStateToEveryone(STATE_RESTART_ELECTION)
				WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_RESTART_ELECTION})
			} 

			if random > -mv {
				// ON EST LEADER
				LEADER = true
				break
			}
		} else { // Sinon mv est positif
			if mv > random { // Si il y a un pair qui a une grosse valeur seul 
				// On est pas leader mais il y a un leader
				break
			}

			if mv == random {
				SendStateToEveryone(STATE_RESTART_ELECTION)
				WaitingForEveryoneOnTheSpecifiedStates([]int{STATE_RESTART_ELECTION})
			}
			
			if random > mv {
				// ON EST LEADER
				LEADER = true
				break
			}
		}
	}

	if LEADER == true {
		roles := GiveRoles()
		SendToEveryone("role", roles)
	} else {
		for role == "" {}
	}
}

/*
* Methode permettant au leader de faire la distribution les roles
*/
func GiveRoles() string {
	roles := ""
	indice := 0
	mux.Lock()
	for ip_pair, pair := range reg.IPClient {
		if indice >= len(rolesDispos) {
			indice = indice - len(rolesDispos)
		}
		pair.Role = rolesDispos[indice]
		reg.IPClient[ip_pair] = pair
		if roles != "" {
			roles = roles + ";"
		}
		roles = roles + ip_pair + "-" + rolesDispos[indice]
		indice = indice +1
	}
	mux.Unlock()
	
	if indice >= len(rolesDispos) {
		indice = indice - len(rolesDispos)
	}
	role = rolesDispos[indice]
	roles = roles + ";" + ip_for_friend + "-" + role

	return roles
}

/*
* Methode permettant de noter les roles de tous les pairs dans notre registre
*/
func NoteRoles(roles []string)  {
	for _, role_pair := range roles {
		ip_role := strings.Split(role_pair, "-")
		if ip_role[0] != ip_for_friend {
			mux.Lock()
			pair := reg.IPClient[ip_role[0]]
			pair.Role = ip_role[1]
			reg.IPClient[ip_role[0]] = pair
			mux.Unlock()
		} else {
			role = ip_role[1]
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												Petites fonctions utiles												//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
* Methode permettant de demander a l'utilisateur d'écrire
* title:string => Message affiché dans la console
* @return string => la réponse
*/
func AskForAnswer(title string) string {
	fmt.Print("\n" + title)
	var res string
	fmt.Scanln(&res)
	return res
}

/*
* Methode permettant de récupérer son addresse ip
* @return string => la réponse
*/
func GetIpAddress() string {
	conn, _ := net.Dial("udp", "8.8.8.8:80")
	defer conn.Close()
	return strings.Split(conn.LocalAddr().(*net.UDPAddr).String(), ":")[0]
}

/*
* Methode permettant récuperer une ip depuis une classe de type Addr
*/
func GetIpFromConn(addr net.Addr) string {
	re := regexp.MustCompile(`:\d+`)
	ip := re.Split(addr.String(), -1)[0]
	return ip
}

/*
* Methode permettant d'afficher les informations en début de partie
*/
func DisplayAtTheBeginning(disp_str string) {
	fmt.Println("\n------------------------------------")
	fmt.Println(disp_str)
	fmt.Println("\n	Pour inviter des amis,")
	fmt.Println("	Envoyez leur cette adresse IP : ", ip_for_friend)
	fmt.Println("\n------------------------------------")
	fmt.Print("\n\n IP DE VOTRE HOST : ", ip_host)
	fmt.Println("\n Votre pseudo est : ", pseudo)
	fmt.Println()
}